/*
Copyright (c) 2013, Texas State University-San Marcos. All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted for academic, research, experimental, or personal use provided
that the following conditions are met:

   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of Texas State University-San Marcos nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

For all other uses, please contact the Office for Commercialization and Industry
Relations at Texas State University-San Marcos <http://www.txstate.edu/ocir/>.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

Authors: Martin Burtscher and Hassan Rabeti
*/


/******************************************************************************/
/*** ILCS Parallelization Framework C Code ************************************/
/******************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <assert.h>
#include <math.h>
#include <string.h>
#include <limits.h>
#include <unistd.h>
#include <sys/time.h>
#include <omp.h>
#include "ilcs.h"
#ifdef ILCS_USE_MPI
#include <mpi.h>
#endif

#ifdef ILCS_USE_GPUS
int ilcs_get_num_gpus();
size_t ilcs_init_gpus(int argc, char *argv[], int gpus);
long ilcs_exec_gpu(int deviceID, long seed, long stride, void const* champion, void* result);
void ilcs_output_gpu(void const* champion);
#endif

int main(int argc, char *argv[])
{
  int i, comm_sz, my_rank, threads;
  int step;
  size_t size;
#ifdef ILCS_USE_GPUS
  size_t gpusize;
#endif
  long lCPUs, gCPUs, lGPUs, gGPUs;
  volatile long CPUevals, GPUevals;
  long **best_result;
  struct timeval starttime, endtime;

  if (sizeof(long) < 8) {fprintf(stderr, "size of long is too short\n"); exit(-1);}
  if (ILCS_SLEEP_TIME < 1) {fprintf(stderr, "ILCS_SLEEP_TIME needs to be at least 1 s\n"); exit(-1);}
  if (ILCS_NO_CHANGE_THRESHOLD < 1) {fprintf(stderr, "ILCS_NO_CHANGE_THRESHOLD needs to be at least 1\n"); exit(-1);}

  comm_sz = 1;
  my_rank = 0;
#ifdef ILCS_USE_MPI
  MPI_Init(NULL, NULL);
  MPI_Comm_size(MPI_COMM_WORLD, &comm_sz);
  MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);
#endif

  lCPUs = 0;
#ifdef ILCS_USE_CPUS
  lCPUs = omp_get_num_procs();
#endif

  lGPUs = 0;
#ifdef ILCS_USE_GPUS
  lGPUs = ilcs_get_num_gpus();
#endif

  gCPUs = lCPUs;
  gGPUs = lGPUs;
#ifdef ILCS_USE_MPI
  MPI_Reduce(&lCPUs, &gCPUs, 1, MPI_LONG, MPI_SUM, 0, MPI_COMM_WORLD);
  MPI_Reduce(&lGPUs, &gGPUs, 1, MPI_LONG, MPI_SUM, 0, MPI_COMM_WORLD);
#endif

  if (my_rank == 0) {
    fprintf(stderr, "\nILCS Framework v0.9\n");
    fprintf(stderr, "Copyright (c) 2013, Texas State University-San Marcos. All rights reserved.\n");
    fprintf(stderr, "configuration: %d processes, %d sec/step, %d steps to termination\n", comm_sz, ILCS_SLEEP_TIME, ILCS_NO_CHANGE_THRESHOLD);
    fprintf(stderr, "configuration: %ld CPUs/node, %ld CPUs total\n", lCPUs, gCPUs);
    fprintf(stderr, "configuration: %ld GPUs/node, %ld GPUs total\n", lGPUs, gGPUs);
  }

  size = 0;
#ifdef ILCS_USE_CPUS
  size = CPU_Init(argc, argv);
#endif
#ifdef ILCS_USE_GPUS
  gpusize = ilcs_init_gpus(argc, argv, lGPUs);
#ifdef ILCS_USE_CPUS
  if (gpusize != size) {fprintf(stderr, "CPU and GPU Init code must return the same size\n"); exit(-1);}
#endif
  size = gpusize;
#endif
  if (size < 1) {fprintf(stderr, "Init code must be called and must return a size greater than zero\n"); exit(-1);}

  while ((size % sizeof(long)) != 0) {
    size++;
  }

  threads = lCPUs + lGPUs;
  best_result = (long **)malloc(threads * sizeof(long *));
  if (best_result == NULL) {fprintf(stderr, "could not allocate memory\n"); exit(-1);}

  for (i = 0; i < threads; i++) {
    best_result[i] = (long *)malloc(size);
    if (best_result[i] == NULL) {fprintf(stderr, "could not allocate memory\n"); exit(-1);}
    best_result[i][0] = LONG_MAX;
  }

  long minimum = LONG_MAX;
  long lmin, gmin;
  volatile int cont = 1;
  volatile long *champ = NULL;
  CPUevals = 0;
  GPUevals = 0;

#ifdef ILCS_USE_MPI
  MPI_Barrier(MPI_COMM_WORLD);  // for better timing
#endif

  gettimeofday(&starttime, NULL);
#pragma omp parallel num_threads(threads + 1) default(shared)
  {
    if (omp_get_num_threads() != threads + 1) {fprintf(stderr, "not enough threads launched\n"); exit(-1);}

    int rank = omp_get_thread_num();
    if (rank > 0) {
      rank--;
      if (rank < lCPUs) {  // CPU threads
#ifdef ILCS_USE_CPUS
        long seed = LONG_MAX / comm_sz * my_rank + rank;
        long *res = (long *)malloc(size);
        if (res == NULL) {fprintf(stderr, "could not allocate memory\n"); exit(-1);}
        do {
          CPU_Exec(seed, (void*)champ, res);
#pragma omp atomic
          CPUevals++;
          if (best_result[rank][0] > res[0]) {
#pragma omp critical
            memcpy(best_result[rank], res, size);
          }
          seed += lCPUs;
        } while (cont);
#endif
#ifdef ILCS_USE_GPUS
      } else {  // GPU handler threads
        int deviceID = rank - lCPUs;
        long seed = LONG_MAX / comm_sz * my_rank - deviceID - 1;
        long *res = (long *)malloc(size);
        if (res == NULL) {fprintf(stderr, "could not allocate memory\n"); exit(-1);}
        do {
          long evaluations;
          evaluations = ilcs_exec_gpu(deviceID, seed, lGPUs, (void*)champ, res);
#pragma omp atomic
          GPUevals += evaluations;
          if (best_result[rank][0] > res[0]) {
#pragma omp critical
            memcpy(best_result[rank], res, size);
          }
          seed -= lGPUs * evaluations;
        } while (cont);
#endif
      }
    } else {  // communication thread
      int nochange = 0;
      step = 0;
      do {
        sleep((ILCS_SLEEP_TIME + 3) / 4);
        gettimeofday(&endtime, NULL);
        double runtime = endtime.tv_sec + endtime.tv_usec/1000000.0 - starttime.tv_sec - starttime.tv_usec/1000000.0;
        if (runtime > (step + 1) * ILCS_SLEEP_TIME) {
          step++;
          lmin = best_result[0][0];
          int best = 0;
          for (i = 1; i < threads; i++) {
            if (lmin > best_result[i][0]) {
              lmin = best_result[i][0];
              best = i;
            }
          }
          gmin = lmin;
#ifdef ILCS_USE_MPI
          MPI_Allreduce(&lmin, &gmin, 1, MPI_LONG, MPI_MIN, MPI_COMM_WORLD);
#endif
          nochange++;
          if (minimum > gmin) {
            minimum = gmin;
            nochange = 0;

            long *temp = (long *)malloc(size);
            if (temp == NULL) {fprintf(stderr, "could not allocate memory\n"); exit(-1);}
#ifdef ILCS_USE_MPI
            if (minimum == lmin) {
              lmin = my_rank;
            } else {
              lmin = LONG_MAX;
            }
            MPI_Allreduce(&lmin, &gmin, 1, MPI_LONG, MPI_MIN, MPI_COMM_WORLD);
            if (my_rank == gmin) {
#pragma omp critical
              memcpy(temp, best_result[best], size);
            }
            MPI_Bcast(temp, size, MPI_BYTE, gmin, MPI_COMM_WORLD);
#else
#pragma omp critical
            memcpy(temp, best_result[best], size);
#endif
            champ = temp;
            if (my_rank == 0) {
              fprintf(stderr, "step: %d  ", step);  fflush(stderr);
#ifdef ILCS_USE_CPUS
//              CPU_Output((void*)champ);
#else
//              ilcs_output_gpu((void*)champ);
#endif
            }
          }
        }
      } while (nochange < ILCS_NO_CHANGE_THRESHOLD);
      cont = 0;  // signal to stop other threads
    }
  }

  lmin = best_result[0][0];
  int best = 0;
  for (i = 1; i < threads; i++) {
    if (lmin > best_result[i][0]) {
      lmin = best_result[i][0];
      best = i;
    }
  }
  gmin = lmin;
#ifdef ILCS_USE_MPI
  MPI_Allreduce(&lmin, &gmin, 1, MPI_LONG, MPI_MIN, MPI_COMM_WORLD);
#endif
  if (minimum > gmin) {
    minimum = gmin;
    long *temp = (long *)malloc(size);
    if (temp == NULL) {fprintf(stderr, "could not allocate memory\n"); exit(-1);}
#ifdef ILCS_USE_MPI
    if (minimum == lmin) {
      lmin = my_rank;
    } else {
      lmin = LONG_MAX;
    }
    MPI_Allreduce(&lmin, &gmin, 1, MPI_LONG, MPI_MIN, MPI_COMM_WORLD);
    if (my_rank == gmin) {
#pragma omp critical
      memcpy(temp, best_result[best], size);
    }
    MPI_Bcast(temp, size, MPI_BYTE, gmin, MPI_COMM_WORLD);
#else
#pragma omp critical
    memcpy(temp, best_result[best], size);
#endif
    champ = temp;
  }
  gettimeofday(&endtime, NULL);

  long CPUtotal = CPUevals;
  long GPUtotal = GPUevals;
#ifdef ILCS_USE_MPI
  MPI_Reduce((long*)&CPUevals, &CPUtotal, 1, MPI_LONG, MPI_SUM, 0, MPI_COMM_WORLD);
  MPI_Reduce((long*)&GPUevals, &GPUtotal, 1, MPI_LONG, MPI_SUM, 0, MPI_COMM_WORLD);
#endif

  if (my_rank == 0) {
#ifdef ILCS_USE_CPUS
    CPU_Output((void*)champ);
#else
    ilcs_output_gpu((void*)champ);
#endif
    double runtime = endtime.tv_sec + endtime.tv_usec/1000000.0 - starttime.tv_sec - starttime.tv_usec/1000000.0;
    fprintf(stderr, "\nruntime: %.3lf sec\n", runtime);
    fprintf(stderr, "steps: %d\n", step);
    fprintf(stderr, "CPUtotal: %ld (%.1lf per sec)\n", CPUtotal, CPUtotal / runtime);
    fprintf(stderr, "GPUtotal: %ld (%.1lf per sec)\n", GPUtotal, GPUtotal / runtime);
    fprintf(stderr, "overall : %ld (%.1lf per sec)\n", CPUtotal + GPUtotal, (CPUtotal + GPUtotal) / runtime);
  }

#ifdef ILCS_USE_MPI
  MPI_Finalize();
#endif

  return 0;
}


/******************************************************************************/
/*** User C Code **************************************************************/
/******************************************************************************/

#define MAXVARS 201
#define MAXCLAUSES 1001

//Defines a 3SAT clause
typedef struct 
{
   int first;
   int second;
   int third;
} clause;

static int vars;
static int clauses;
static clause sat_problem[MAXCLAUSES];

//Defines the assignments for all the variables in a 3SAT problem
typedef struct 
{
  long quality;  // lower is better
  bool assignments[MAXVARS + 1];
} SAT;

size_t CPU_Init(int argc, char* argv[])
{
   register int i, ch, cnt;
   int i1, i2, i3;
   register FILE *f;
   char str[256];

   if (argc != 2) {fprintf(stderr, "\narguments: input_file\n"); exit(-1);}

   f = fopen(argv[1], "r+t");
   if (f == NULL) {fprintf(stderr, "could not open file %s\n", argv[1]); exit(-1);}

   //Skip the first 3 lines
   ch = getc(f);  while ((ch != EOF) && (ch != '\n')) ch = getc(f);
   ch = getc(f);  while ((ch != EOF) && (ch != '\n')) ch = getc(f);
   ch = getc(f);  while ((ch != EOF) && (ch != '\n')) ch = getc(f);

   //The fourth line should be "Variables: number_of_vars_here"
   ch = getc(f);  while ((ch != EOF) && (ch != ':')) ch = getc(f);
   fscanf(f, "%s\n", str);
   vars = atoi(str);
   if (vars == 0) {
      fprintf(stderr, "%d variables\n", vars);
      exit(-1);
   }
   if (vars >= MAXVARS) {
      fprintf(stderr, "%d variables is too large\n", vars);
      exit(-1);
   }

   //The fifth line should be "Clauses: number_of_clauses_here"
   ch = getc(f);  while ((ch != EOF) && (ch != ':')) ch = getc(f);
   fscanf(f, "%s\n", str);
   clauses = atoi(str);
   if (clauses == 0) {
      fprintf(stderr, "%d clauses\n", clauses);
      exit(-1);
   }
   if (clauses >= MAXCLAUSES) {
      fprintf(stderr, "%d clauses is too large\n", clauses);
      exit(-1);
   }

   //Empty line should be next.

   //Next line should just be the word "CLAUSES"
   fscanf(f, "%s\n", str);
   if (strcmp(str, "CLAUSES") != 0) {
      fprintf(stderr, "wrong file format\n");
      fprintf(stderr, "%s %d %d\n", str, vars, clauses);
      exit(-1);
   }


   //Read in the clauses from the file
   cnt = 0;
   while (fscanf(f, "%d\t\t%d\t\t%d\n", &i1, &i2, &i3)) {
      sat_problem[cnt].first = i1;
      sat_problem[cnt].second = i2;
      sat_problem[cnt].third = i3;
      cnt++;
      if (cnt > clauses) {fprintf(stderr, "input too long\n"); exit(-1);}
   }
   if (cnt != clauses) {
      fprintf(stderr, "read %d instead of %d clauses\n", cnt, clauses);
      exit(-1);
   }

   //Last line of the file should just be "EOF"
   fscanf(f, "%s", str);
   if (strcmp(str, "EOF") != 0) {
      fprintf(stderr, "didn't see 'EOF' at end of file\n");
      exit(-1);
   }
   fclose(f);

   return sizeof(SAT);
}



/*
 * Determines whether the given clause is satisfied
 * by the given solution.
 */
bool satisfied(SAT* solution, clause* c)
{
   //3SAT has clauses of the form (x OR y OR z) AND ...
   //Thus we only need one variable to evaluate to true in the clause.
   if (c->first > 0 && solution->assignments[c->first] == true ||
       c->first < 0 && solution->assignments[abs(c->first)] == false ||
       c->second > 0 && solution->assignments[c->second] == true ||
       c->second < 0 && solution->assignments[abs(c->second)] == false ||
       c->third > 0 && solution->assignments[c->third] == true ||
       c->third < 0 && solution->assignments[abs(c->third)] == false)
   {
      return true;
   }
   else
   {
      return false;
   }
       
}

/*
 * Determines how many clauses in the global variable "problem"
 * are satisfied by the given set of assignments.
 */
int satisfied_clauses(SAT* solution)
{
   register int i, count;
   
   count = 0;
   for (i = 0; i < clauses; i++)
   {
      if (satisfied(solution, &sat_problem[i]))
      {
	 count++;
      }
   }

   return count;
}

void CPU_Exec(long seed, void const* champion, void* result)
{
   register int i, t, best, check, chng;
   unsigned int rndstate;
   register SAT* res = (SAT *)result;

   //Generate a pseudorandom solution based on the seed.
   rndstate = (unsigned int)(seed ^ (seed >> 22) ^ (seed >> 43));
   for (i = 1; i <= vars; i++)
   {
      t = rand_r(&rndstate) % 2;  //0 is false, 1 is true
      res->assignments[i] = t; 
   }

   best = satisfied_clauses(res);

   //Optimization phase: go through the assignments and try flipping them one at a time.
   //Keep the change if it results in more satisfied clauses.
   //We don't need to do any deeper optimization for this to work.
   do 
   {
      chng = 0;
      for (i = 1; i <= vars; i++)
      {
	 res->assignments[i] = !(res->assignments[i]);
	 check = satisfied_clauses(res);
	 if (check > best)
	 {
	    best = check; //keep the assignment change, update best
	    chng++;
	 }
	 else
	 {
	    res->assignments[i] = !(res->assignments[i]); //revert the assignment
	 }
      }

   }
   while (chng > 0);

   //Lower quality is better, so compute (total clauses) - (clauses satisfied) by best solution.
   //So if 200 out of 300 are satisfied, the quality would be 100,
   //but if only 50 were satisfied, the quality would be 250.
   res->quality = clauses - best;
}





void CPU_Output(void const* champion)
{
   register int i;
   register SAT* champ = (SAT *)champion;

   if (champ != NULL) {
    printf("\n\nClauses satisfied: %ld  \nAssignments:", clauses - champ->quality);
    for (i = 1; i <= vars; i++) {
      printf(" %d", (int)champ->assignments[i]);
    }
    printf("\n\n");
  }
}


