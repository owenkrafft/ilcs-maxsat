#
# Makefile for ILCS Framework
#

all:	SAT

SAT:	ilcs0.9.c ilcs.h
	mpicc -fopenmp -O3 ilcs0.9.c -o SAT

gordon: ilcs0.9.c ilcsTSP ilcs.h
	mpicc -openmp -O3 ilcs0.9.c -o SATG
	mpicc -openmp -O3 ilcsTSP.c -o TSPG

TSP:	ilcsTSP.c ilcs.h
	mpicc -fopenmp -O3 ilcsTSP.c -o TSP