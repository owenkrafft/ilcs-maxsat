# ILCS-SAT #

An implementation of a MAX-SAT solver in the ILCS parallelization framework.

I say "solver" but it's more like "throw a bunch of guesses at the problem and report the best one we found"